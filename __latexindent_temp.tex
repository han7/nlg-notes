\documentclass[11pt]{article}

\usepackage[english]{babel} %include this always
\usepackage{amsmath} %ams mathematical stuff
\usepackage[utf8]{inputenc} %smart input of funny chars
\usepackage[T1]{fontenc} %also for the font encoding
\usepackage{longtable} %tables longer than one page
\usepackage{exscale} %large summation signs in 11pt
\usepackage[final]{graphicx} %to include pdf pictures
\usepackage[sort]{cite} %nicer citations
\usepackage{array} %nice tables
\usepackage{wasysym} %smiley symbols
\usepackage{fancyhdr} %nices headers
\usepackage[a4paper,top=2cm,bottom=2.5cm,left=3cm,right=2.5cm,marginparwidth=1.75cm]{geometry} %geometry of page layout
\usepackage{gitinfo2} %git info
\usepackage[draft]{fixme} %correction notes, warnings, etc.
\usepackage{xspace} %better spacing after macros
\usepackage{tikz} %pictures (optional)
\usepackage{ifdraft} %determine whether draft mode
\usepackage{makeidx}
\usepackage[expansion=false]{microtype} %no font expansion but protrusion
\usepackage[nottoc]{tocbibind} %refs and index in the toc
\usepackage[backref=page,final=true,pdfpagelabels]{hyperref} %backrefs in the bibliography always treat as final, use pdf page labels, hyperrefs always at the end
\usepackage{cancel}
\usepackage{float}
\usepackage[final]{showkeys}

\allowdisplaybreaks[1]

\newtheorem{definition}{Definition}
\newtheorem{lemma}[definition]{Lemma}
\newtheorem{theorem}[definition]{Theorem}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{remark}[definition]{Remark}

\newcommand{\LieAlg}[1]{\mathfrak{#1}}
\newcommand{\Banach}[1]{\mathfrak{#1}}
\newcommand{\Hilbert}[1]{\mathfrak{#1}}
\newcommand{\algebra}[1]{\mathscr{#1}}
\newcommand{\domain}[1]{\mathfrak{D}_{#1}}
\newcommand{\Operators}[1]{\mathcal{O}(\mathfrak{#1})}
\newcommand{\innerprod}[1]{\left\langle #1\right\rangle}
\newcommand{\EnvAlg}[1]{\mathscr{#1}}
\newcommand{\anadom}{\overset{\omega}{\leq}}
\newcommand{\at}[1]{|_{#1}}
\newcommand{\image}[1]{\mathrm{im}(#1)}
\newcommand{\graph}[1]{\mathrm{graph}(#1)}

\title{Unbounded Operators and Nelson's Theorem}

%@article{Nelson:1959a,
%	author = {Nelson, E.},
%	title = {Analytic Vectors},
%	journal = {Ann. of Math.},
%	volume = {70},
%	number = {3},
%	pages = {572 -- 615},
%	year = {1959},
%}

%@article{Pierrot:2006a,
%	author = {Pierrot, F.},
%	title = {Op\'{e}rateurs r\'{e}guliers dans les C*-modules et structure des C*-alg\`{e}bres de groupes de Lie semisimples complexes simplement connexes},
%	journal = {Journal of Lie Theory},
%	volume = {16},
%	pages = {651 -- 689},
%	year = {2006},
%}

%@article{Vassout:2006a,
%	author = {Vassout, S.},
%	title = {Unbounded Pseudodifferential Calculus on Lie Groupoids},
%	journal = {Journal of Functional Analysis},
%	volume = {236},
%	pages = {161 -- 200},
%	year = {2006},
%}


\begin{document}

\maketitle

This chapter will be more related to functional analysis as it is dealing with unbounded operators. However, in the end we will give a theorem found by Nelson in \cite{Nelson:1959a}, which integrates a Lie algebra representation to a unitary Lie group representation iff the Laplace operator is essentially self-adjoint. All the notions just mentioned will be defined properly in the following. We first start with the definition of unbounded operators and some of their properties, before we introduce the absolute value of an operator, which will be crucial for estimations on the way to Nelson's theorem.

Note that everything, which is done in the following, can also be established for Hilbert modules as it was shown in \cite{Pierrot:2006a}.

\section{Unbounded Operators and Analytic Vectors}

In the following we will introduce (unbounded) operators, which crucially depend on their domain. Because of this it is not possible to apply an operator on every vector in the Banach or Hilbert space. However, in case of operators that map their domain on subspaces, which are not part of the domain, it is not even possible to square the operator. It is the number of repeated applications of the operator, which gives us a definition for smooth and analytic operators. Moreover, in case of a Hilbert space there are even more detailed possibilities to define unbounded operators similar to bounded ones.

\begin{definition}[Unbounded Operator]
	Let $\Banach{X}$ be a Banach space. Then an unbounded operator on $\Banach{X}$ is given by a linear map
	\begin{equation}
		A\colon\domain{A}\longrightarrow\Banach{X},
	\end{equation}
	where $\domain{A}\subseteq\Banach{X}$ is a linear subspace that is the domain of $A$. The operator $A$ is called densely defined if $\domain{A}$ is a dense subspace of $\Banach{X}$. 
	The set of (unbounded) operators on $\Banach{X}$ is denoted by $\Operators{X}$.
\end{definition}

\begin{remark}
	Compared with bounded (or continuous) linear operators, unbounded operators do not yield an algebra. The reason is the domain which does not coincide for all the operators in $\Operators{X}$.
	This leads to the situation that addition and composition of operators can not be defined as usual. Let $A,B\in\Operators{X}$ be two unbounded operators, then the domain of $A+B$ is given by 
	\begin{equation}
		\domain{A+B} = \domain{A}\cap\domain{B}
	\end{equation}
	and 
	\begin{equation}
		\domain{AB} = \{x\in\Banach{X}\mid Bx\in\domain{A}\text{ for }x\in\domain{B}\}.
	\end{equation} 
	As the domain of an unbounded operators is crucial we will assume every operator to be unbounded in the following. This means they all have an associated domain, which is not always mentioned.
\end{remark}

\begin{example}[Differentiation operator]
	\label{example differentiation as unbounded operator}
	The most famous example and the reason why unbounded operators are considered, is the fact that the usual differentiation operator $\frac{\mathrm{d}}{\mathrm{d} x}$ is an unbounded one. By taking into account the space of continuous functions $C([0,1])$ on the unit interval endowed with the supremum norm 
	\begin{equation}
		\norm{f} = \sup_{x\in[0,1]}\abs{f(x)}
	\end{equation}we can consider the function $f(x) = x^n$ for $n\in\mathbb{N}$. Then we obtain
	\begin{equation}
		\frac{\\mathrm{d} f}{\mathrm{d} x}(x) = nx^{n-1},
	\end{equation}
	which is indeed unbounded by using operator norm.
\end{example}

\begin{definition}[Extension and closure]
	Let $\Banach{X}$ be a Banach space and let $A,B\in\Operators{X}$. 
	\begin{itemize}
		\item[(i)] The operator $A$ is called extension of $B$ if
		\begin{equation}
		\domain{B}\subseteq\domain{A}\qquad\text{ and }\qquad A\at{\domain{B}} = B.
		\end{equation}
		This is denoted by $B\subseteq A$.
		\item[(ii)] The operators $A$ is called closed if its graph 
		\begin{equation}
		\graph{A} = \{(x,Ax)\in\domain{A}\times\Banach{X}\}\subseteq\Banach{X}\times\Banach{X}
		\end{equation} 
		is closed.
		\item[(iii)] The operator $A$ is called closable if it has a closed extension, which is denoted by $\overline{A}$.
		\item[(iv)]  The domain $\domain{A}$ is called invariant if $\image{A}\subseteq\domain{A}$.
	\end{itemize} 
\end{definition}

\begin{remark}
	Let $A$ be an unbounded operator on $\Banach{X}$, which is bounded on its domain, i.e.
	\begin{equation}
		\norm{A} = \sup_{x\in\Banach{X}}\frac{\norm{Ax}}{\norm{x}}<\infty.
	\end{equation}
	Then there is a unique and bounded extension of $A$ on $\domain{A}^{\mathrm{cl}}$. Especially, if $A$ is a densely defined operator then $\domain{A}^{\mathrm{cl}} = \Banach{X}$ and thus $A$ becomes a bounded operator on the whole Banach space $\Banach{X}$. On the other hand if there is a bounded extension on $\domain{A}^{\mathrm{cl}} = \Banach{X}$, then there $A$ must already be a bounded operator on $\domain{A}$.
\end{remark}

\begin{definition}[Smooth and analytic vectors]
	Let $\Banach{X}$ be a Banach space and let $A\in\Operators{X}$ be an unbounded operator.
	\begin{itemize}
		\item[(i)] The vector $x\in\domain{A}$ is called smooth for $A$ if $x\in\domain{A^k}$ for all $k\in\mathbb{N}$ and the set of smooth vectors for $A$ is denoted by $\domain{A}^\infty$.
		\item[(ii)] The vector $x\in\domain{A}$ is called analytic for $A$ if there is an $s>0$ such that
			\begin{equation}
				\label{equation definition of analytic vector}
				\sum_{n=0}^{\infty}\frac{\norm{A^nx}}{n!}s^n <\infty.
			\end{equation}
			The set of analytic vectors for $A$ is denoted by $\domain{A}^{\omega}$ and the set of analytic vectors for $A$ with respect to one $s>0$ is denoted by $\domain{A}^{\omega,s}$. 
	\end{itemize}
\end{definition}

\begin{remark}
	Let $A\in\Operators{X}$ be an operator on a Banach space $\Banach{X}$. 
	\begin{itemize}
		\item[(i)] Then the smooth vectors for $A$ are equivalently given by the intersection of all $\domain{A^k}$ for $k\in\mathbb{N}$, i.e.
		\begin{equation}
		\domain{A}^\infty = \bigcap_{k=0}^\infty\domain{A^k}.
		\end{equation}
		Moreover, note that every analytic vector for $A$ is contained in the set of smooth vectors for $A$, i.e. $\domain{A}^\omega\subseteq\domain{A}^\infty$. The reason is that only in this case the series is well-defined.
		\item[(ii)] Note that the condition given in Equation (\ref{equation definition of analytic vector}) is the same as requiring $e^{sA}x$ to have a positive radius of absolute value.
	\end{itemize}
\end{remark}

\begin{example}[Analytic functions]
	\label{example analytic functions}
	Considering the same conditions as in Example \ref{example differentiation as unbounded operator}, i.e. the Banach space is given by $C([0,1])$ endowed with the supremum norm. Then the analytic vectors with respect to the differentiation operator $\frac{\mathrm{d}}{\mathrm{d} x}$ are given by the analytic functions on the interval $[0,1]$.
\end{example}

So far we only considered the case of a Banach space $\Banach{X}$. In case of a Hilbert space $\Hilbert{H}$ with its inner-product $\innerprod{\cdot,\cdot}$ there is even more structure for unbounded operators like symmetric and adjoint operators. However, it is not as easy as in the case of bounded operators to define an adjoint of an operator since we need to be very careful with the domains of the operators. For the following definition we also switch to densely defined operators.

\begin{definition}[Symmetric operator]
	Let $\Hilbert{H}$ be a Hilbert space and let $A$ be a densely defined operator on $\Hilbert{H}$.
	\begin{itemize}
		\item[(i)] The operator $A$ is called symmetric if 
		\begin{equation}
			\innerprod{x,Ay} = \innerprod{Ax,y}
		\end{equation}
		for all $x,y\in\domain{A}$.
		\item[(ii)] The operator $A$ is called skew-symmetric if
		\begin{equation}
			\innerprod{x,Ay} = -\innerprod{Ax,y}
		\end{equation}
		for all $x,y\in\domain{A}$.
	\end{itemize}
\end{definition}

\begin{remark}
	Note that there is still another definition for symmetry for a densely defined operator $A$ on a Hilbert space which only requires the adjoint operator $A^*$ to be an extension of $A$, i.e. $A\subseteq A^*$. Indeed it turns out that we also arrive at
	\begin{equation}
	\innerprod{x,Ay} = \innerprod{Ax,y}
	\end{equation}
	for all $x,y\in\domain{A}$.
\end{remark}

\begin{definition}[Adjoint operator]
	Let $\Hilbert{H}$ be a Hilbert space and let $A$ be a densely defined operator on $\Hilbert{H}$.
	\begin{itemize}
		\item[(i)] The operator $A^*$ is called the adjoint of $A$, if it is defined on the domain
		\begin{equation}
		\domain{A^*} = \{x\in\Hilbert{H}\mid y\mapsto\innerprod{x,Ay}\text{ is continuous}\}
		\end{equation}
		such that
		\begin{equation}
		\innerprod{x,Ay} = \innerprod{A^*x,y}
		\end{equation}
		for all $y\in\domain{A}$.
		\item[(ii)] If $A=A^*$, then $A$ is called self-adjoint.
		\item[(iii)] If $A = -A^*$, then $A$ is called skew-adjoint.
		\item[(iv)] If $\overline{A} = A^*$, then $A$ is called essentially self-adjoint.
		\item[(v)] If $\overline{A} = -A^*$, then $A$ is called essentially skew-adjoint.
	\end{itemize}
\end{definition}

\begin{remark}
	There must be some discussion on the definition of the adjoint of an operator $A$. First note that due to the continuity of the map $y\mapsto\innerprod{x,Ay}$ is a continuous linear functional on $\domain{A}$, but as this is densely contained in $\Hilbert{H}$ it has a bounded extension on $\Hilbert{H}$. By Riesz theorem it follows that there is a $z\in\Hilbert{H}$ such that $\innerprod{z,y} = \innerprod{x,Ay}$ for all $y\in\domain{A}$. Again, the densely contained domain leads to the existence of a unique operator satisfying $z = A^*x$. 
\end{remark}



\section{Calculus of Absolute Value}

At this point the actual journey starts aiming at Nelson's theorem. This is neatly presented in Nelson's paper \cite{Nelson:1959a}. Nevertheless, we will give a motivation and a step-by-step explanation of (almost) everything but will refer to this mentioned paper for the unimportant details. The reason for talking about the estimations is the following: suppose $A,X\in\Operators{X}$ such that $\norm{Xx}\leq\norm{Ax}$ for all $x\in\domain{A+X}$, then there is a theorem, which gives conditions for all analytic vectors of $A$ being analytic vectors of $X$. However, this involves certain estimations of $\norm{X^n x}$ in terms of $\norm{x},\norm{Ax},\dots,\norm{A^n x}$. For two commuting operators it turns out that $\norm{X^nx}\leq\norm{A^nx}$ for all $n\in\mathbb{N}$, but this does not holds in the general case. Actually, we need to take into account the commutator to be able to obtain such kind of estimation. Note that in the following $A$ will be an elliptic operator and $X$ denotes a first order operator in the enveloping algebra of operators. 

In the following we will establish some rules to rewrite the inequality
\begin{equation}
	\norm{Cx} \leq \norm{Ax} + \norm{Bx}
\end{equation}
for all $x\in\domain{A}\cap\domain{B}\cap\domain{C}$ by
\begin{equation}
	\abs{C} \leq \abs{A} + \abs{B}
\end{equation} 
for some operators $A,B,C\in\Operators{X}$. 

\begin{definition}[Absolute value of an operator]
	Let $\Banach{X}$ be a Banach space and let $A\in\Operators{X}$. The symbol $\abs{A}$ is called the absolute value of $A$. The set of absolute values $\abs{\Operators{X}}$ is linearly generated by the formal finite sum of absolute values of all the operators contained in $\Operators{X}$.
\end{definition}

\begin{proposition}
	Let $\Banach{X}$ be a Banach space. 
	\begin{itemize}
		\item[(i)] The space of absolute values $\abs{\Operators{X}}$ yields a abelian semigroup.
		\item[(ii)] For absolute values $\alpha = \abs{A_1} + \dots + \abs{A_l}$ and $\beta = \abs{B_1} + \dots + \abs{B_m}$ the multiplication defined by
		\begin{equation}
			\label{equation multiplication of absolute values}
			\alpha\beta = \sum_{i=1}^{l}\sum_{j=1}^{m}\abs{A_iB_j}
		\end{equation}
		makes $\abs{\Operators{X}}$ a semialgebra by the identification of positive numbers $a$ with $\abs{a\mathbb{1}}$ for $\mathbb{1}$ being the identity.
	\end{itemize}
\end{proposition}

\begin{definition}[Norm of absolute value]
	Let $\Banach{X}$ be a Banach space and let $\alpha,\beta\in\abs{\Operators{X}}$. The norm of an absolute value $\alpha = \abs{A_1}+\dots+\abs{A_l}$ is defined by
	\begin{equation}
		\norm{\alpha x} = \norm{A_1x}+\dots+\norm{A_lx}
	\end{equation}
	for all $x\in\Banach{X}$ by using the convention $\norm{Ax} = \infty$ for $x\notin\domain{A}$. For another absolute value $\beta = \abs{B_1}+\dots+\abs{B_m}$ the ordering $\alpha\leq\beta$ is defined by
	\begin{equation}
		\norm{\alpha x}\leq\norm{\beta x}
	\end{equation}
	for all $x\in\Banach{X}$.
\end{definition}

\begin{proposition}
	Let $\Banach{X}$ be a Banach space and $A,B,C\in\Operators{X}$.
	\begin{itemize}
		\item[(i)] The inequality	$\abs{A + B} \leq \abs{A} + \abs{B}$ holds true.
		\item[(ii)] If $\abs{A}\leq\abs{B}$ holds then $\abs{AC}\leq\abs{BC}$ holds.
	\end{itemize}
	
	\begin{proof}
		By setting $\alpha = \abs{A}$ and $\beta = \abs{B}$ the claims hold true.
	\end{proof}
\end{proposition}

\begin{definition}[Power series of absolute values]
	Let $\Banach{X}$ be a Banach space and let $(\alpha_n)_{n\in\mathbb{N}}$ and $(\beta_n)_{n\in\mathbb{N}}\subset\abs{\Operators{X}}$. 
	\begin{itemize}
		\item[(i)] A power series of absolute values $\varphi$ is given by
		\begin{equation}
			\varphi = \sum_{n=0}^{\infty}\alpha_n s^n.
		\end{equation}
		For another power series of absolute value $\psi$ with coefficients $(\beta_n)_{n\in\mathbb{N}}$ the ordering $\varphi\leq\psi$ is defined by $\alpha_n\leq\beta_n$ for all $n\in\mathbb{N}$. 
		\item[(ii)] The norm of a power series of absolute value $\varphi$ coming from $(\alpha_n)_{n\in\mathbb{N}}$ is defined for all $x\in\Banach{X}$ by
		\begin{equation}
			\norm{\varphi x} = \sum_{n=0}^{\infty} \norm{\alpha_nx}s^n.
		\end{equation}
	\end{itemize}
\end{definition}

\begin{definition}[Analytic vector for absolute value]
	Let $\Banach{X}$ be a Banach space. A vector $x\in\Banach{X}$ is called an analytic vector for $\alpha\in\abs{\Operators{X}}$ if there is some $s>0$ such that
	\begin{equation}
		\label{equation analytic vector for abs value}
		\norm{e^{s\alpha}x}<\infty.
	\end{equation}
	The set of analytic vector for $\alpha$ in denoted by $\domain{\alpha}^\omega$ and the set of analytic vectors for $\alpha$ for a particular $s>0$ is denoted by $\domain{\alpha}^{\omega,s}$.
\end{definition}

\begin{remark}
	Note that in case of $\alpha = \abs{A}$ we arrive at
	\begin{equation}
		\norm{e^{s\abs{A}}x} = \sum_{n=0}^{\infty}\frac{\norm{A^n x}}{n!}s^n,
	\end{equation}
	which means that in this case $x\in\domain{\abs{A}}^\omega$ is also an analytic vector for $A$.	
\end{remark}

The commutator of operators $A,X\in\Operators{X}$ is given by
\begin{equation}
	(\mathrm{ad} X)A = XA - AX.
\end{equation}
Hence we define the commutator of absolute value in the following way.

\begin{definition}[Commutator of absolute value]
	Let $\Banach{X}$ be a Banach space and $\alpha,\xi\in\abs{\Operators{X}}$ with $\alpha = \abs{A_1} + \dots + \abs{A_l}$ and $\xi = \abs{X_1}+\dots+\abs{X_d}$. The commutator is defined by
	\begin{equation}
		(\mathrm{ad}\xi)\alpha = \sum_{i=1}^{d}\sum_{j=1}^{l}\abs{X_iA_j - A_jX_i}.
	\end{equation}
\end{definition}

\begin{theorem}
	\label{theorem analytical domination}
	Let $\Banach{X}$ be a Banach space and let $\alpha,\xi\in\abs{\Operators{X}}$ like before. Let $\xi\leq c\alpha$, $(\mathrm{ad}\xi)^n\alpha\leq c_n\alpha$ and 
	\begin{equation}
		\nu(s) = \sum_{n=1}^\infty\frac{c_n}{n!}s^n\qquad\text{ and }\qquad\kappa(s) = \int_0^s\frac{\D t}{1-\nu(t)}.
	\end{equation}
	Then $e^{s\xi} \leq e^{c\kappa(s)\alpha}$.
\end{theorem}

\begin{remark}
	In order to give the estimation
	\begin{equation}
		(\mathrm{ad}\xi)^n\alpha \leq c_n\alpha
	\end{equation}
	in the previous theorem, it is nessessary to require $A$ to be elliptic. 
\end{remark}

\begin{definition}[Analytic dominance]
	Let $\Banach{X}$ be a Banach space and let $\alpha,\xi\in\abs{\Operators{X}}$ be related as in Theorem \ref{theorem analytical domination} such that $c,c_n<\infty$ and $\nu(s)$ has positive convergence radius. Then it is said that $\alpha$ analytically dominates $\xi$, which is denoted by $\xi\overset{\omega}{\leq}\alpha$.
\end{definition}

\begin{corollary}
	Let $\Banach{X}$ be a Banach space and let $\alpha,\xi\in\abs{\Operators{X}}$. If $\xi\anadom\alpha$ then $\domain{\alpha}^\omega\subseteq\domain{\xi}^\omega$.
\end{corollary}

\begin{remark}
	Note that so far we did not use the completeness of the Banach space $\Banach{X}$ we always have available. Therefore it would be sufficient to consider only a normed vector space. Furthermore, we did not even use the linearity of the operators $\Operators{X}$.
\end{remark}


\section{Nelson's Theorem}

From now on let $\Hilbert{H}$ be a Hilbert space. In then next two lemmas we are aiming to find statements about domains of extended corresponding operators. The problem is that for two operators $A$ and $X$ having closures with $\abs{X}\anadom\abs{A}$, it is not clear that in case $x\in\domain{\overline{A}}^\omega$ we can conclude $x\in\domain{\overline{X}^2}^\omega$.

\begin{proposition}
	\label{proposition Nelson's theorem}
	Let $X\in\Operators{H}$ be a closed and symmetric operator on a Hilbert space $\Hilbert{H}$. Then $X$ is self-adjoint iff $\domain{X}^\omega\subseteq\Hilbert{H}$ is dense.
\end{proposition}

This equivalence says that we only need enough analytic vectors for the operator $X$, which are contained in the Hilbert space $\Hilbert{H}$ to make it a self-adjoint operator.

\begin{remark}
	\begin{itemize}
		\item[(i)] The previous statement still holds true if "symmetric" is replaced by "skew-symmetric" and "self-adjoint" by "skew-adjoint" by considering $\I X$, which has the same set of analytic vectors as $X$.
		\item[(ii)] The statement having the condition of $\domain{X}^\omega$ as a dense subspace in Proposition \ref{proposition Nelson's theorem} is also said to be Nelson's theorem. 
	\end{itemize}
\end{remark}

\begin{lemma}
	\label{lemma domain in case of analytic dominance}
	Let $X_1,\dots,X_d,A\in\Operators{H}$ be symmetric operators on a Hilbert space $\Hilbert{H}$ with common invariant domain $\domain{}$ and suppose that $A$ is essentially self-adjoint.
	Let $\xi = \abs{X_1} + \dots + \abs{X_d}$, $\alpha = \abs{A} + \abs{\mathbb{1}}$, $\xi \leq c\alpha$ and $(\mathrm{ad}\xi)^n\alpha \leq c_n\alpha$ with $c<\infty$ and $c_n < \infty$ for all $n \geq 1$. For all finite sequences $i_1,\dots,i_n$ one has
	\begin{equation}
		\domain{\overline{A}^n}\subset\domain{\overline{X}_{i_1}\dots\overline{X}_{i_n}}.
	\end{equation}
	Let $\tilde{\domain{}} = \bigcap_{n = 1}^{\infty}\domain{\overline{A}^n}$ and let $\widetilde{X}_1,\dots,\widetilde{X}_d,\widetilde{A}$ be the restrictions of $\overline{X}_1,\dots,\overline{X}_d,\overline{A}$ to $\widetilde{\domain{}}$. Let $\widetilde{\xi} = \abs{\widetilde{X}_1} + \dots + \abs{\widetilde{X}_d}$, $\alpha = \abs{\widetilde{A}} + \abs{\mathbb{1}}$. Then $\widetilde{\xi} \leq c\widetilde{\alpha}$, $(\mathrm{ad}\widetilde{\xi})^n\widetilde{\alpha} \leq c_n\widetilde{\alpha}$ for all $n\geq 1$.
	
	Moreover, if $\xi\anadom\alpha$, then there is an $s>0$ such that the set $\domain{\widetilde{\xi}}^{\omega,s}\subseteq\domain{}$ is dense in $\Hilbert{H}$ and each $X_i$ is essentially self-adjoint.
\end{lemma}

\begin{remark}
	The essence of this lemma is that we can find a common subspace $\tilde{\domain{}}$, which the closed operators satisfy the same estimations on as given by the original operators before. Moreover, in case of analytic dominance the self-adjointness of the dominating operator passes to other such that they become essentially self-adjoint.
\end{remark}

In the following $\domain{}\subseteq\Hilbert{H}$ be a subspace and $\LieAlg{g}$ be a Lie algebra consisting of the skew-symmetric operators having $\domain{}$ as a common invariant domain. The Lie bracket of $\LieAlg{g}$ is the usual commutator, which maps into the skew-symmetric operators as well.

Let us denote the universal enveloping algebra of $\LieAlg{g}$ by $\algebra{A}$. An element of $\algebra{A}$ is said to be of order $\leq n$ if it consists of a real linear combination of operators of the form $Y_1\cdots Y_k$ with $k\leq n$ and all $Y_j\in\LieAlg{g}$. The set of elements of order $\leq n$ is denoted by $\algebra{A}_n$ and make $\algebra{A}$ a filtered algebra, i.e. $\algebra{A}_0\subseteq\algebra{A}_1\subseteq\dots\subseteq\algebra{A}_n\subseteq\dots$ and $\algebra{A}_k\cdot\algebra{A}_l\subseteq\algebra{A}_{k+l}$. The element $\Delta$ of order 2 is also called \textit{Nelson's Laplacian} and given by
\begin{equation}
	\Delta = X_1^2 + \dots + X_d^2
\end{equation}
for $X_1,\dots,X_d$ forming a basis of the Lie algebra $\LieAlg{g}$. The goal in the following is to establish some estimations in terms of Nelson's Laplacian $\Delta$.

\begin{lemma}
	For $B\in\algebra{A}_2$ there is some $k<\infty$ such that $\abs{B}\leq k\abs{\Delta-\mathbb{1}}$.
\end{lemma}

\begin{lemma}
	\label{lemma analytic dominance of xi}
	Let $\xi = \abs{X_1} + \dots + \abs{X_d}$ and let $\alpha = \abs{\Delta - \mathbb{1}}$. Then $\xi\anadom\alpha$, especially, $\xi \leq \sqrt{\frac{d}{2}}\alpha$ and there is a $c<\infty$ such that for all $n \geq 1$, $(\mathrm{ad}\xi)^n\alpha \leq c^n\alpha$. Furthermore, $\xi\anadom\abs{\Delta} + \abs{\mathbb{1}}$.
\end{lemma}

\begin{lemma}
	Let $m\in\mathbb{N}$. If $B\in\EnvAlg{A}_{2m}$, then for some $k<\infty$ one has
	\begin{equation}
		\abs{B} \leq k\alpha^m,
	\end{equation}
	where $\alpha^m = \abs{(\Delta - \mathbb{1})^m}$. If $\eta = \abs{Y_1} + \dots + \abs{Y_l}$ for $Y_j\in\EnvAlg{A}_{2m}$ and $\mathrm{ad} Y_j$ maps into $\EnvAlg{A}_{2m}$ into itself, for $j = 1,\dots,l$, then $\eta\anadom\alpha^m$ for some $c<\infty$, $(\mathrm{ad}\eta)^n\alpha^m \leq c^n\alpha^m$ for all $n\geq 1$.
\end{lemma}

In the following $G$ be a simply-connected Lie group of the Lie algebra $\LieAlg{g}$ leading to an answer for the question: when does a representation of $\LieAlg{g}$ come from a unitary representation of $G$? The next statement gives a relation of unitary representation of $G$ if there are enough analytic vectors.

\begin{lemma}
	\label{lemma unitary repr bec of analytic vectors}
	Let $\LieAlg{g}$ be a Lie algebra of skew-symmetric operators on a Hilbert space $\Hilbert{H}$ having a common invariant domain $\domain{}$. Let $X_1,\dots,X_d$ be a basis for $\LieAlg{g}$ and let $\xi = \abs{X_1} + \dots + \abs{X_d}$. If for some $s>0$ the set $\domain{\xi}^{\omega,s}\subseteq\domain{}$ is dense in $\Hilbert{H}$, then there is on $\Hilbert{H}$ a unique unitary representation $U$ of the simply-connected Lie group $G$ having $\LieAlg{g}$ as a Lie algebra such that for all $X\in\LieAlg{g}$, $\overline{U(X)} = \overline{X}$.
\end{lemma}

\begin{theorem}
	Let $\LieAlg{g}$ be a Lie algebra of skew-symmetric operators on a Hilbert space $\Hilbert{H}$ having a common domain $\domain{}$. Let $X_1,\dots,X_d$ be a basis for $\LieAlg{g}$ and $\Delta = X_1^2 + \dots + X_d^2$. If $\Delta$ is essentially self-adjoint, then there is on $\Hilbert{H}$ a unique unitary representation $U$ of the simply-connected Lie group $G$ having $\LieAlg{g}$ as its Lie algebra such that for all $X\in\LieAlg{g}$ one has $\overline{U(X)} = \overline{X}$.
	
	\begin{proof}
		For $\xi = \abs{X_1} + \dots + \abs{X_d}$ we know by Lemma \ref{lemma analytic dominance of xi} that $\xi\anadom\abs{\Delta} + \abs{\mathbb{1}}$ and thus by Lemma \ref{lemma domain in case of analytic dominance} we have $\domain{\tilde{xi}}^{\omega,s}\subseteq\Hilbert{H}$ is dense. Note that for applying Lemma \ref{lemma domain in case of analytic dominance} Nelson's Laplacian needs to be essentially self-adjoint. By Lemma \ref{lemma unitary repr bec of analytic vectors} we obtain the unitary representation we were looking for.
		
	\end{proof}
\end{theorem}

\section{Sobolev Spaces}

In this section we introduce abstract Sobolev spaces occuring in connection with Nelson's Laplacian $\Delta$, where Nelson's Laplacian is supposed to be essentially self-adjoint. Thus it is closed and symmetric.

%%% Motivation in terms of Fourier transform and spectral calculus is missing!!!!!!!!!!!!

\begin{definition}[s-th Sobolev space,\cite{Vassout:2006a}]
	Let $\Hilbert{H}$ be a Hilbert space and $\LieAlg{g}$ the Lie algebra of skew-symmetric operators. Let $X_1,\dots,X_d\in\LieAlg{g}$ be a basis and $\Delta = X_1^2 + \dots + X_d^2$ be essentially self-adjoint. Then for $s>0$ the pre-Hilbert space
	\begin{equation}
		\Hilbert{S}^s = \overline{\domain{\overline{\Delta}^{s/2}}}
	\end{equation}
	with the scalar product given by
	\begin{equation}
		\label{equation scalar product for Sobolev space}
		\innerprod{x,y}_s = \innerprod{x,y} + \innerprod{\overline{\Delta}^{s/2}x,\overline{\Delta}^{s/2}y}
	\end{equation}
	for $x,y\in\Hilbert{S}^s$ is called the s-th Soboloev space of $\Delta$.
\end{definition}

\begin{proposition}
	Let $\Hilbert{S}^s$ be the s-th Sobolev space for some $s>0$.
	\begin{itemize}
		\item[(i)] The s-th Sobolev space $\Hilbert{S}^s$ is a Hilbert space with respect to the scalar product given in Equation (\ref{equation scalar product for Sobolev space}).
		\item[(ii)] An operator $\mathsf{P}\in\Operators{\Hilbert{H}}$ of order $m$ extends to a linear bounded map 
		\begin{equation}
			\mathsf{P}\colon\Hilbert{S}^s\longrightarrow\Hilbert{S}^{s-m}.
		\end{equation}
	\end{itemize}
\end{proposition}
\end{document}